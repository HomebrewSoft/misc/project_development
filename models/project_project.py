# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class Project(models.Model):
    _inherit = 'project.project'

    odoo_version = fields.Selection(
        selection=[
            ('8.0', '8.0'),
            ('9.0', '9.0'),
            ('10.0', '10.0'),
            ('11.0', '11.0'),
            ('12.0', '12.0'),
            ('13.0', '13.0'),
        ],
    )
    enterprise = fields.Boolean(
    )
    repo_url = fields.Char(
    )
    type_ids = fields.Many2many(
        default=lambda self: [
            self.env.ref('project_development.project_task_type_specification').id,
            self.env.ref('project_development.project_task_type_back-log').id,
            self.env.ref('project_development.project_task_type_development').id,
            self.env.ref('project_development.project_task_type_test').id,
            self.env.ref('project_development.project_task_type_done').id,
        ],
    )
