# -*- coding: utf-8 -*-
{
    'name': 'Project Development',
    'version': '13.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'project',
    ],
    'data': [
        # security
        # data
        'data/project_task_type.xml',
        # reports
        # views
        'views/project_project.xml',
        'views/project_task.xml',
    ],
}
